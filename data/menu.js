/*
 * Main and demo navigation arrays
 */

export default {
  'main': [
    {
      name: 'Dashboard',
      to: '/dashboard',
      icon: 'si si-speedometer'
    },
    {
      name: 'Page Packs',
      icon: 'si si-layers',
      subActivePaths: '/pages-packs',
      sub: [
        {
          name: 'Boxed Backend',
          icon: 'si si-magnet',
          subActivePaths: 'boxed',
          sub: [
            {
              name: 'Dashboard',
              to: '/pages/boxed/dashboard'
            },
            {
              name: 'Search',
              to: '/pages/boxed/search'
            },
            {
              name: 'Simple 1',
              to: '/pages/boxed/simple1'
            },
            {
              name: 'Simple 2',
              to: '/pages/boxed/simple2'
            },
            {
              name: 'Image 1',
              to: '/pages/boxed/image1'
            },
            {
              name: 'Image 2',
              to: '/pages/boxed/image2'
            }
          ]
        }
      ]
    },
    {
      name: 'User Interface',
      heading: true
    },
    {
      name: 'Blocks',
      icon: 'si si-energy',
      subActivePaths: '/blocks',
      sub: [
        {
          name: 'Styles',
          to: '/blocks/styles'
        },
        {
          name: 'Options',
          to: '/blocks/options'
        },
        {
          name: 'Forms',
          to: '/blocks/forms'
        },
        {
          name: 'Themed',
          to: '/blocks/themed'
        },
        {
          name: 'API',
          to: '/blocks/api'
        }
      ]
    },
    {
      name: 'Elements',
      icon: 'si si-badge',
      subActivePaths: '/elements',
      sub: [
        {
          name: 'Grid',
          to: '/elements/grid'
        },
        {
          name: 'Typography',
          to: '/elements/typography'
        },
        {
          name: 'Icons',
          to: '/elements/icons'
        },
        {
          name: 'Buttons',
          to: '/elements/buttons'
        },
        {
          name: 'Button Groups',
          to: '/elements/button-groups'
        },
        {
          name: 'Dropdowns',
          to: '/elements/dropdowns'
        },
        {
          name: 'Tabs',
          to: '/elements/tabs'
        },
        {
          name: 'Navigation',
          to: '/elements/navigation'
        },
        {
          name: 'Horizontal Navigation',
          to: '/elements/navigation-horizontal'
        },
        {
          name: 'Progress',
          to: '/elements/progress'
        },
        {
          name: 'Alerts',
          to: '/elements/alerts'
        },
        {
          name: 'Tooltips',
          to: '/elements/tooltips'
        },
        {
          name: 'Popovers',
          to: '/elements/popovers'
        },
        {
          name: 'Modals',
          to: '/elements/modals'
        },
        {
          name: 'Images',
          to: '/elements/images'
        },
        {
          name: 'Timeline',
          to: '/elements/timeline'
        },
        {
          name: 'Ribbons',
          to: '/elements/ribbons'
        },
        {
          name: 'Animations',
          to: '/elements/animations'
        },
        {
          name: 'Color Themes',
          to: '/elements/color-themes'
        }
      ]
    },
    {
      name: 'Tables',
      icon: 'si si-grid',
      subActivePaths: '/tables',
      sub: [
        {
          name: 'Styles',
          to: '/tables/styles'
        },
        {
          name: 'Responsive',
          to: '/tables/responsive'
        },
        {
          name: 'Helpers',
          to: '/tables/helpers'
        },
        {
          name: 'Pricing',
          to: '/tables/pricing'
        }
      ]
    },
    {
      name: 'Forms',
      icon: 'si si-note',
      subActivePaths: '/forms',
      sub: [
        {
          name: 'Elements',
          to: '/forms/elements'
        },
        {
          name: 'Custom Controls',
          to: '/forms/custom-controls'
        },
        {
          name: 'Layouts',
          to: '/forms/layouts'
        },
        {
          name: 'Input Groups',
          to: '/forms/input-groups'
        },
        {
          name: 'Plugins',
          to: '/forms/plugins'
        },
        {
          name: 'Editors',
          to: '/forms/editors'
        },
        {
          name: 'Validation',
          to: '/forms/validation'
        }
      ]
    },
    {
      name: 'Develop',
      heading: true
    },
    {
      name: 'Plugins',
      icon: 'si si-wrench',
      subActivePaths: '/plugins',
      sub: [
        {
          name: 'Appear',
          to: '/plugins/appear'
        },
        {
          name: 'Image Cropper',
          to: '/plugins/image-cropper'
        },
        {
          name: 'Charts',
          to: '/plugins/charts'
        },
        {
          name: 'Calendar',
          to: '/plugins/calendar'
        },
        {
          name: 'Carousel',
          to: '/plugins/carousel'
        },
        {
          name: 'Syntax Highlighting',
          to: '/plugins/syntax-highlighting'
        },
        {
          name: 'Rating',
          to: '/plugins/rating'
        },
        {
          name: 'Dialogs',
          to: '/plugins/dialogs'
        },
        {
          name: 'Notifications',
          to: '/plugins/notifications'
        },
        {
          name: 'Gallery',
          to: '/plugins/gallery'
        }
      ]
    },
    {
      name: 'Layout',
      icon: 'si si-magic-wand',
      subActivePaths: '/layout',
      sub: [
        {
          name: 'Page',
          subActivePaths: '/layout/page',
          sub: [
            {
              name: 'Default',
              to: '/layout/page/default'
            },
            {
              name: 'Flipped',
              to: '/layout/page/flipped'
            }
          ]
        },
        {
          name: 'Main Content',
          subActivePaths: '/layout/main-content',
          sub: [
            {
              name: 'Full Width',
              to: '/layout/main-content/full-width'
            },
            {
              name: 'Narrow',
              to: '/layout/main-content/narrow'
            },
            {
              name: 'Boxed',
              to: '/layout/main-content/boxed'
            }
          ]
        },
        {
          name: 'Header',
          subActivePaths: '/layout/header',
          sub: [
            {
              name: 'Fixed',
              heading: true
            },
            {
              name: 'Light',
              to: '/layout/header/fixed-light'
            },
            {
              name: 'Dark',
              to: '/layout/header/fixed-dark'
            },
            {
              name: 'Static',
              heading: true
            },
            {
              name: 'Light',
              to: '/layout/header/static-light'
            },
            {
              name: 'Dark',
              to: '/layout/header/static-dark'
            }
          ]
        },
        {
          name: 'Sidebar',
          subActivePaths: '/layout/sidebar',
          sub: [
            {
              name: 'Mini',
              to: '/layout/sidebar/mini'
            },
            {
              name: 'Light',
              to: '/layout/sidebar/light'
            },
            {
              name: 'Dark',
              to: '/layout/sidebar/dark'
            },
            {
              name: 'Hidden',
              to: '/layout/sidebar/hidden'
            }
          ]
        },
        {
          name: 'Side Overlay',
          subActivePaths: '/layout/side-overlay',
          sub: [
            {
              name: 'Visible',
              to: '/layout/side-overlay/visible'
            },
            {
              name: 'Hover Mode',
              to: '/layout/side-overlay/hover-mode'
            },
            {
              name: 'No Page Overlay',
              to: '/layout/side-overlay/no-page-overlay'
            }
          ]
        },
        {
          name: 'Loaders',
          to: '/layout/loaders'
        },
        {
          name: 'API',
          to: '/layout/api'
        }
      ]
    },
    {
      name: 'Multi Level Menu',
      icon: 'si si-puzzle',
      sub: [
        {
          name: 'Link 1-1',
          to: '#'
        },
        {
          name: 'Link 1-2',
          to: '#'
        },
        {
          name: 'Sub Level 2',
          sub: [
            {
              name: 'Link 2-1',
              to: '#'
            },
            {
              name: 'Link 2-2',
              to: '#'
            },
            {
              name: 'Sub Level 3',
              sub: [
                {
                  name: 'Link 4-1',
                  to: '#'
                },
                {
                  name: 'Link 4-2',
                  to: '#'
                },
                {
                  name: 'Sub Level 5',
                  sub: [
                    {
                      name: 'Link 5-1',
                      to: '#'
                    },
                    {
                      name: 'Link 5-2',
                      to: '#'
                    },
                    {
                      name: 'Sub Level 6',
                      sub: [
                        {
                          name: 'Link 6-1',
                          to: '#'
                        },
                        {
                          name: 'Link 6-2',
                          to: '#'
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    },
    {
      name: 'Pages',
      heading: true
    },
    {
      name: 'Generic',
      icon: 'si si-layers',
      subActivePaths: '/pages/generic',
      sub: [
        {
          name: 'Blank',
          to: '/pages/generic/blank'
        },
        {
          name: 'Blank (Block)',
          to: '/pages/generic/blank-block'
        },
        {
          name: 'Search',
          to: '/pages/generic/search'
        },
        {
          name: 'Profile',
          to: '/pages/generic/profile'
        },
        {
          name: 'Invoice',
          to: '/pages/generic/invoice'
        },
        {
          name: 'FAQ',
          to: '/pages/generic/faq'
        },
        {
          name: 'Inbox',
          to: '/pages/generic/inbox'
        },
        {
          name: 'Maintenance',
          to: '/pages/various/maintenance'
        },
        {
          name: 'Status',
          to: '/pages/various/status'
        },
        {
          name: 'Coming Soon',
          to: '/pages/various/coming-soon'
        }
      ]
    },
    {
      name: 'Authentication',
      icon: 'si si-lock',
      subActivePaths: '/pages/auth',
      sub: [
        {
          name: 'All',
          to: '/pages/auth/all'
        },
        {
          name: 'Sign In',
          to: '/pages/auth/signin'
        },
        {
          name: 'Sign In 2',
          to: '/pages/auth/signin2'
        },
        {
          name: 'Sign In 3',
          to: '/pages/auth/signin3'
        },
        {
          name: 'Sign Up',
          to: '/pages/auth/signup'
        },
        {
          name: 'Sign Up 2',
          to: '/pages/auth/signup2'
        },
        {
          name: 'Sign Up 3',
          to: '/pages/auth/signup3'
        },
        {
          name: 'Lock Screen',
          to: '/pages/auth/lock'
        },
        {
          name: 'Lock Screen 2',
          to: '/pages/auth/lock2'
        },
        {
          name: 'Lock Screen 3',
          to: '/pages/auth/lock3'
        },
        {
          name: 'Pass Reminder',
          to: '/pages/auth/reminder'
        },
        {
          name: 'Pass Reminder 2',
          to: '/pages/auth/reminder2'
        },
        {
          name: 'Pass Reminder 3',
          to: '/pages/auth/reminder3'
        }
      ]
    },
    {
      name: 'Error Pages',
      icon: 'si si-fire',
      subActivePaths: '/pages/errors',
      sub: [
        {
          name: 'All',
          to: '/pages/errors/all'
        },
        {
          name: '400',
          to: '/pages/errors/400'
        },
        {
          name: '401',
          to: '/pages/errors/401'
        },
        {
          name: '403',
          to: '/pages/errors/403'
        },
        {
          name: '404',
          to: '/pages/errors/404'
        },
        {
          name: '500',
          to: '/pages/errors/500'
        },
        {
          name: '503',
          to: '/pages/errors/503'
        }
      ]
    }
  ],
  'demo': [
    {
      name: 'Home',
      to: '#',
      icon: 'fa fa-home',
      badge: 5
    },
    {
      name: 'Manage',
      heading: true
    },
    {
      name: 'Products',
      icon: 'fa fa-briefcase',
      sub: [
        {
          name: 'HTML Templates',
          icon: 'fab fa-html5',
          sub: [
            {
              name: 'Description',
              to: '#',
              icon: 'fa fa-pencil-alt'
            },
            {
              name: 'Statistics',
              to: '#',
              icon: 'fa fa-chart-line'
            },
            {
              name: 'Sales',
              to: '#',
              icon: 'fa fa-chart-area',
              badge: 320
            },
            {
              name: 'Media',
              to: '#',
              icon: 'far fa-images',
              badge: 18
            },
            {
              name: 'Files',
              to: '#',
              icon: 'far fa-file-alt',
              badge: 32
            }
          ]
        },
        {
          name: 'WordPress Themes',
          icon: 'fab fa-wordpress',
          sub: [
            {
              name: 'Description',
              to: '#',
              icon: 'fa fa-pencil-alt'
            },
            {
              name: 'Statistics',
              to: '#',
              icon: 'fa fa-chart-line'
            },
            {
              name: 'Sales',
              to: '#',
              icon: 'fa fa-chart-area',
              badge: 680
            },
            {
              name: 'Media',
              to: '#',
              icon: 'far fa-images',
              badge: 15
            },
            {
              name: 'Files',
              to: '#',
              icon: 'far fa-file-alt',
              badge: 20
            }
          ]
        },
        {
          name: 'Web Applications',
          icon: 'fab fa-medapps',
          sub: [
            {
              name: 'Description',
              to: '#',
              icon: 'fa fa-pencil-alt'
            },
            {
              name: 'Statistics',
              to: '#',
              icon: 'fa fa-chart-line'
            },
            {
              name: 'Sales',
              to: '#',
              icon: 'fa fa-chart-area',
              badge: 158
            },
            {
              name: 'Media',
              to: '#',
              icon: 'far fa-images',
              badge: 65
            },
            {
              name: 'Files',
              to: '#',
              icon: 'far fa-file-alt',
              badge: 78
            }
          ]
        },
        {
          name: 'Video Templates',
          icon: 'fab fa-youtube',
          sub: [
            {
              name: 'Description',
              to: '#',
              icon: 'fa fa-pencil-alt'
            },
            {
              name: 'Statistics',
              to: '#',
              icon: 'fa fa-chart-line'
            },
            {
              name: 'Sales',
              to: '#',
              icon: 'fa fa-chart-area',
              badge: 920
            },
            {
              name: 'Media',
              to: '#',
              icon: 'far fa-images',
              badge: 7
            },
            {
              name: 'Files',
              to: '#',
              icon: 'far fa-file-alt',
              badge: 19
            }
          ]
        },
        {
          name: 'Add Product',
          to: '#',
          icon: 'fa fa-plus'
        }
      ]
    },
    {
      name: 'Payments',
      icon: 'fa fa-money-bill-wave',
      sub: [
        {
          name: 'Scheduled',
          to: '#',
          badge: 3,
          'badge-variant': 'success'
        },
        {
          name: 'Recurring',
          to: '#'
        },
        {
          name: 'Manage',
          to: '#'
        },
        {
          name: 'New Payment',
          to: '#',
          icon: 'fa fa-plus'
        }
      ]
    },
    {
      name: 'Services',
      icon: 'fa fa-globe-americas',
      sub: [
        {
          name: 'Hosting',
          to: '#'
        },
        {
          name: 'Web Design',
          to: '#'
        },
        {
          name: 'Web Development',
          to: '#'
        },
        {
          name: 'Graphic Design',
          to: '#'
        },
        {
          name: 'Legal',
          to: '#'
        },
        {
          name: 'Consulting',
          to: '#'
        }
      ]
    },
    {
      name: 'Personal',
      heading: true
    },
    {
      name: 'Profile',
      icon: 'far fa-user',
      sub: [
        {
          name: 'Edit',
          to: '#'
        },
        {
          name: 'Settings',
          to: '#'
        },
        {
          name: 'Log out',
          to: '#'
        }
      ]
    }
  ]
}
